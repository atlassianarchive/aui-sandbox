AUI Sandbox
===========

The sandbox is a place where you can play around with existing components and mock up new components using AUI without having to setup a dev environment.

Installing
----------

Clone the repository then run:

    npm install

Running
-------

To run the sandbox you need the soy serer and http server running.

    grunt soy-server
    grunt http-server

Testing
-------

To run the unit tests:

    grunt test

If you want to run them in a watch server:

    grunt test-watch

Deploying
---------

If they haven't been set yet you'll need to set environment vars (https://devcenter.heroku.com/articles/config-vars):

    SOY_HOST=atl-soy-compiler-service.herokuapp.com
    SOY_PORT=80
