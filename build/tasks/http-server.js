'use strict';

module.exports = function(grunt) {
    grunt.registerMultiTask('http-server', 'Runs a sandbox development server.', function() {
        var done = this.async();
        var http = require('http');
        var fs = require('fs');
        var request = require('request');
        var express = require('express');
        var Q = require('q');
        var sandbox_server = express();
        var options = this.options({
            host: 'localhost',
            dbport: 5984,
            db: 'aui-sandbox',
            keepalive: true,
            port: 9000,
            soyAUIInitPath: '/rest/compile/aui',
            soyCompileRequestPath: '/rest/compile/multiple',
            soyHost: 'localhost',
            soyPort: 9001
        });

        request('http://' + options.host + ':' + options.dbport, function(err, response, data) {
            if(err){
                console.error("ERROR: CouchDB Not found");
            }
        });

        sandbox_server.use(express.bodyParser());

        sandbox_server.get('/:version/demos', function(req, res) {
            var demosPromise = getDemos(req.params.version);

            demosPromise.done(function(data){
                res.writeHead('200', { 'Content-Type': 'application/json' });
                res.end(data, 'utf-8');
            });
        });

        // TODO: This can go at the top once the code to serve SaveCtrl-Live is removed.
        sandbox_server.use(express.static(process.cwd()));

        // TODO: Remove once express.static is executed before the router.
        sandbox_server.get('/component-output.css', function(req, res) {
            fs.readFile('component-output.css', function(error, content) {
                res.writeHead(200, { 'Content-Type': 'text/css' });
                res.end(content, 'utf-8');
            });
        });

        // TODO: Remove once express.static is executed before the router.
        sandbox_server.get('/component-output.js', function(req, res) {
            fs.readFile('component-output.js', function(error, content) {
                res.writeHead(200, { 'Content-Type': 'application/javascript' });
                res.end(content, 'utf-8');
            });
        });

        sandbox_server.get('/code/:hash-:rev', function(req, res) {
            getCodeRevision(req.params.hash, req.params.rev, res);
        });

        sandbox_server.get('/code/:hash', function(req, res) {
            getCode(req.params.hash, null, res);
        });

        sandbox_server.get('/:hash', function(req, res) {
            fs.readFile('index.html', function(error, content) {
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.end(content, 'utf-8');
            });
        });

        sandbox_server.post('/soy', function(req, res) {
            var auiOnly = req.body.auiOnly;
            var template = req.body.template;

            if (auiOnly) {
                var promise = sendSoyCompileRequest({
                    path: options.soyAUIInitPath,
                    requestMethod: 'GET'
                });
            } else if (template) {
                var promise = sendSoyCompileRequest({
                    path: options.soyCompileRequestPath,
                    requestMethod: 'POST',
                    template: template
                });
            }

            promise.done(function (data) {
                if (typeof data.data === 'string') {
                    res.writeHead(data.statusCode, { 'Content-Type': 'text/plain' });
                    res.end(data.data, 'utf-8');
                }
            });
        });

        sandbox_server.post('/code', function(req, res) {
            var saveData = {
                js: req.body.js,
                css: req.body.css,
                html: req.body.html,
                soy: req.body.soy,
                isSoy: req.body.isSoy,
                version: req.body.version
            };
            generateNewId(function(newId) {
                saveCode(newId, saveData, res);
            });
        });

        sandbox_server.put('/code/:hash', function(req, res) {
            var saveData = {
                js: req.body.js,
                css: req.body.css,
                html: req.body.html,
                soy: req.body.soy ,
                isSoy: req.body.isSoy,
                version: req.body.version
            };
            // find the latest revision
            request({
                url: 'http://' + options.host + ':' + options.dbport + '/' + options.db + '/' + req.params.hash,
                method: 'GET'
            }, function(err, response, data) {
                saveData._rev = JSON.parse(data)._rev;
                saveCode(req.params.hash, saveData, res);
            });
        });

        var port = process.env.PORT || options.port;

        sandbox_server.listen(port, function() {
            grunt.log.writeln('Listening on ' + port);

            if (!options.keepalive) {
                done();
            }
        });

        function demosLocation(version){
            return util.format(CONFIG.demosLocation, version);
        }

        function getDemos(version){
            var deferred = Q.defer();
            request(demosLocation(version), function(error, response, body){
                if(error){
                    deferred.reject();
                } else {
                    deferred.resolve(body);
                }
            });

            return deferred.promise;
        }

        function getCodeRevision(codehash, revision, res) {
            var get_revisions_url = 'http://' + options.host + ':' + options.dbport + '/' + options.db + '/' + codehash + '?revs_info=true';
            request(get_revisions_url, function(err, response, data) {
                var revInfo = JSON.parse(data)._revs_info;
                getCode(codehash, revInfo[revInfo.length-revision].rev, res);
            });
        }

        function getCode(codehash, revision, res) {
            var request_url = 'http://' + options.host + ':' + options.dbport + '/' + options.db + '/' + codehash;

            if (revision) {
                request_url = request_url + '?rev=' + revision;
            }
            request({
                url: request_url,
                method: 'GET'
            }, function(err, response, data) {
                var result = JSON.parse(data),
                    code = JSON.stringify({
                        js: result.js,
                        html: result.html,
                        css: result.css,
                        soy: result.soy,
                        isSoy: result.isSoy,
                        version: result.version
                    });

                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.write(code);
                res.end();
            });
        }

        function saveCode(codehash, code, res) {
            var put_url = 'http://' + options.host + ':' + options.dbport + '/' + options.db + '/' + codehash;
            request({
                url: put_url,
                method: 'PUT',
                body: JSON.stringify(code)
            }, function(err, response, data) {
                var responseObj = JSON.parse(data);
                var obj = { id: responseObj.id };
                var rev = responseObj.rev.split('-')[0];

                if (parseInt(rev) > 1) {
                    obj.rev = rev;
                }

                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.write(JSON.stringify(obj));
                res.end();
            });
        }

        function generateNewId(callback) {
            request('http://' + options.host + ':' + options.dbport + '/_uuids', function(err, response, data) {
                if(!err){
                    var newID = JSON.parse(data).uuids[0];
                    checkID(newID, function(exists, newID) {
                        if (!exists) {
                            if (typeof callback==='function') {
                                callback(newID);
                            }
                        }
                    });
                } else {
                    console.error("ERROR: Could not create new code ID");
                }
            });

        }

        function checkID(id, callback) {
            request('http://' + options.host + ':' + options.dbport + '/' + options.db + '/' + id, function(err, response, data) {
                if (typeof callback==='function') {
                    callback(data.error=='not_found', id);
                }
            });
        }

        /**
         * @param options
         * @param options.template
         * @param options.requestMethod
         * @param options.path
         * @return promise
         */
        function sendSoyCompileRequest(options) {
            var deferred = Q.defer();
            var response = '';

            var postOptions = {
                host: process.env.SOY_HOST || options.soyHost,
                port: process.env.SOY_PORT || options.soyPort,
                path: options.path,
                method: options.requestMethod
            };

            var req = http.request(postOptions);
            req.on('response', function(res) {
                res.setEncoding('utf8');

                res.on('data', function(data) {
                    response += data;
                });

                res.on('end', function() {
                    var map = {
                        data: response,
                        statusCode: res.statusCode
                    };
                    deferred.resolve(map);
                });
            });

            req.on('error', function(data) {
                var map = {
                    data: data,
                    statusCode: 400
                };
                deferred.resolve(map);
            });

            if (options.template) {
                var data = 'template=' + options.template;
                req.write(data);
            }

            req.end();

            return deferred.promise;
        }
    });
};
