#!/usr/bin/perl
use strict;
use warnings;

my $sandboxTemplates = $ARGV[0];
my $sandboxTarget = $ARGV[1];

my $master = "define('registerall', ['MainModule','Template'], function(){\n";

if(!-d $sandboxTarget) {
    mkdir($sandboxTarget);
}

print "Writing to file: " . $sandboxTarget."/registerall.js \n";
my $jsdirectory = $sandboxTemplates . '/js/';
opendir (JSDIR, $jsdirectory) or die $!;

while (my $file = readdir(JSDIR)) {
   if($file ne "." && $file ne "..") {
      print "Building $file...\n";
      open (MYFILE, $jsdirectory.$file);
      $file =~ s/.js//g;
      my $content = "var js = ''\n";
      while (<MYFILE>) {
         chomp;
         s/\r//g;
         s/\'/\\\'/g;
         $content = $content." + '".$_."\\n'\n";
      }
      close (MYFILE);
      $content= $content."SANDBOX.Library.register('$file', 'js', js, 'components')\n\n";
      $master = $master.$content;
      # print $content;
   }
}

my $htmldirectory = $sandboxTemplates . '/html/';
opendir (HTMLDIR, $htmldirectory) or die $!;

while (my $file = readdir(HTMLDIR)) {
   if($file ne "." && $file ne "..") {
      print "Building $file...\n";
      open (MYFILE, $htmldirectory.$file);
      $file =~ s/.html//g;
      my $content = "var html = ''\n";
      while (<MYFILE>) {
         chomp;
         s/\r//g;
         s/\'/\\\'/g;
         $content = $content." + '".$_."\\n'\n";
      }
      close (MYFILE);
      $content= $content."SANDBOX.Library.register('$file', 'html', html, 'components')\n\n";
      $master = $master.$content;
       # print $content;
   }
}

my $soydirectory = $sandboxTemplates . '/soy/';

opendir (SOYDIR, $soydirectory) or die $!;

while (my $file = readdir(SOYDIR)) {
   if($file ne "." && $file ne "..") {
      print "Building $file...\n";
      open (MYFILE, $soydirectory.$file);
      $file =~ s/.soy//g;
      my $content = "var soy = ''\n";
      while (<MYFILE>) {
         chomp;
         s/\r//g;
         s/\'/\\\'/g;
         $content = $content." + '".$_."\\n'\n";
      }
      close (MYFILE);
      $content= $content."SANDBOX.Library.register('$file', 'soy', soy, 'components')\n\n";
      $master = $master.$content;
      # print $content;
   }
}

my $patternshtmldirectory = $sandboxTemplates . '/patterns/html/';

my $isPatternHtml = opendir (PATTERNSHTMLDIR, $patternshtmldirectory);

if($isPatternHtml){
   while (my $file = readdir(PATTERNSHTMLDIR)) {
      if($file ne "." && $file ne "..") {
         print "Building $file...\n";
         open (MYFILE, $patternshtmldirectory.$file);
         $file =~ s/.html//g;
         my $content = "var html = ''\n";
         while (<MYFILE>) {
            chomp;
            s/\r//g;
            s/\'/\\\'/g;
            $content = $content." + '".$_."\\n'\n";
         }
         close (MYFILE);
         $content= $content."SANDBOX.Library .register('$file', 'html', html, 'patterns')\n\n";
         $master = $master.$content;
          # print $content;
      }
   }
}


my $patternsjsdirectory =  $sandboxTemplates . '/patterns/js/';
my $isPatternJs = opendir (PATTERNSJSDIR, $patternsjsdirectory);

if($isPatternJs){
   while (my $file = readdir(PATTERNSJSDIR)) {
      if($file ne "." && $file ne "..") {
         print "Building $file...\n";
         open (MYFILE, $patternsjsdirectory.$file);
         $file =~ s/.js//g;
         my $content = "var js = ''\n";
         while (<MYFILE>) {
            chomp;
            s/\r//g;
            s/\'/\\\'/g;
            $content = $content." + '".$_."\\n'\n";
         }
         close (MYFILE);
         $content= $content."SANDBOX.Library.register('$file', 'js', js, 'patterns')\n\n";
         $master = $master.$content;
         # print $content;
      }
   }
}

$master = $master."});";
my $outfile = $sandboxTarget."/registerall.js";
open (OUTFILE, "> $outfile");
print OUTFILE "$master";
close(OUTFILE);


