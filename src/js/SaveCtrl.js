// Stub SaveCtrl for static sandbox
define(function(sandboxModule) {
    return ['$scope', '$http', 'saveService', 'editorService', '$location', 'componentService', function($scope, $http, saveService, editorService, $location, componentService){
        var componentId = $location.search().component;

        componentService.setCurrentComponent($scope.components[componentId]);
        deferSettingCodeIfComponent();


        function deferSettingCodeIfComponent() {
            for (var a in $scope.components) {
                if (a === componentId) {
                    var component = $scope.components[a];

                    _.defer(function() {
                        $scope.editors.html.setValue(component.html);
                        $scope.editors.css.setValue(component.css);
                        $scope.editors.js.setValue(component.js);
                        $scope.runJavascript();
                    });

                    break;
                }
            }
        }

        $scope.inlineDialog = AJS.InlineDialog(
            AJS.$('#publish-button'),
            1,
            function(contents, trigger, showPopup) {
                contents.append(AJS.$("#url-inline-dialog"));
                showPopup();
            },
            {
                arrowOffsetX: 30,
                noBind: true, width: 400
            }
        );

        $scope.publish = function(){
            var js = editorService.getCode('js');
            var css = editorService.getCode('css');
            var html = editorService.getCode('html');
            var soy = editorService.getCode('soy');
            var isSoy = editorService.isSoyEnabled();
            var pathSplit = $location.path().split("/");
            var id = pathSplit[2] ? pathSplit[2].split("-")[0] : undefined;
            var version = pathSplit[1];

            saveService.publishCode(js, css, html, soy, isSoy, version, id, function(newURL) {
                $scope.changePublishURL(newURL);
                AJS.$('#url-inline-dialog').show();
                $scope.inlineDialog.show();
            });
        }
        var codeId = saveService.readURL($location.absUrl());

    }];
});