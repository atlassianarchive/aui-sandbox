define(function() {
    return [
        '$scope',
        'saveService',
        'editorService',
        'componentService',
        'envService',
        function($scope, saveService, editorService, componentService, envService) {
            var editors = editorService.getEditors();

            $scope.panels = {
                library: {
                    title: 'Library',
                    visible: true,
                    shrunk: false
                },
                html: {
                    style: {
                        left: 0,
                        right: '20%'
                    },
                    title: 'HTML',
                    visible: true,
                    shrunk: false
                },
                soy: {
                    style: {
                        left: '20%',
                        right: '40%'
                    },
                    title: 'Soy',
                    visible: false,
                    shrunk: false
                },
                js: {
                    style: {
                        left: '40%',
                        right: '60%'
                    },
                    title: 'Javascript',
                    visible: true,
                    shrunk: false
                },
                css: {
                    style: {
                        left: '60%',
                        right: '80%'
                    },
                    title: 'CSS',
                    visible: false,
                    shrunk: false
                },
                output: {
                    style: {
                        left: '80%',
                        right: '100%'
                    },
                    title: 'Preview',
                    visible: true,
                    shrunk: false
                }
            };

            $scope.aceInit = function(ed) {
                editorService.init(ed);
            };

            $scope.save = function() {
                AJS.$('#save-wait-icon').removeClass('hidden');

                saveService.newSnippetId();

                var snippet = saveService.saveCode(null, {
                    html: editorService.getCode('html'),
                    css: editorService.getCode('css'),
                    js: editorService.getCode('js'),
                    soy: editorService.getCode('soy')
                });

                $scope.currentSnippetId = snippet.id;
                componentService.setCurrentComponent();

                setTimeout(function() {
                    AJS.$('#save-wait-icon').addClass('hidden');
                }, 250);
            };

            $scope.togglePanel = function(panel) {
                panel.visible = !panel.visible;

                setEditorWidths();
                resizeEditors();
                savePanels();
            };

            $scope.togglePanelExpand = function(panelName) {

                visPanels = getVisiblePanels();
                if (visPanels.length === 1) {
                    restoreStrunkPanels();
                } else {
                    expandPanel(panelName);
                }
                clearSelections();
                setEditorWidths();
                resizeEditors();
                savePanels();
            }

            function restoreStrunkPanels () {
                _.each($scope.panels, function(panel, id) {
                   if (panel.shrunk) {
                        panel.shrunk = false;
                        panel.visible = true;
                   }
                });
            }

            function expandPanel (panelName) {
                _.each($scope.panels, function(panel, id) {
                   if (panel.visible && id != panelName) {
                        panel.shrunk = true;
                        panel.visible = false;
                   } else {
                        panel.shrunk = false;
                   }
                });
            }

            function clearSelections () {
                if (window.getSelection) {
                    window.getSelection().removeAllRanges();
                } else if (document.selection) {
                    document.selection.empty();
                }
            }

            noSoyInProd();
            resizeEditorsOnWindowResize();
            setEditorWidths();
            restoreSavedPanels();
            setupPaneResizing();


            function setupPaneResizing() {
                var $body = $('body');

                $('#sandbox-content-area .splitter-handle').on('mousedown.stretch', function(evt) {
                    var $currentSplitter = $(this);
                    var $currentPanel = $currentSplitter.closest('section.stretch');
                    var $prevPanel = $($currentPanel.prevAll('section.stretch:visible')[0]);
                    var $splitter = $(evt.target);

                    evt.preventDefault();
                    var mouseupHandler = function () {
                        $body.off('mousemove.stretch');
                        $splitter.removeClass('dragging');
                        $('#output-frame').css('pointer-events', 'auto');
                    };

                    $body.on('mousemove.stretch',function (evt) {
                        evt.preventDefault();
                        $('#output-frame').css('pointer-events', 'none');
                        $splitter.addClass('dragging');
                        setPanesWidth($prevPanel, $currentPanel, $splitter, evt.pageX);
                    });

                    $body.one('mouseup mouseleave', mouseupHandler);
                });
            }

            function setPanesWidth($leftPanel, $rightPanel, $splitter, mouseX) {
                var MIN_WIDTH = 100;
                var currentPosition = $splitter.offset().left;
                var diff = mouseX - currentPosition;
                var $stretchPanels = $('.stretch-panels');
                // var percentageDiff = diff/$stretchPanels.width() * 100;
                var currentLeftPanelRight = $stretchPanels.outerWidth() - $leftPanel.position().left - $leftPanel.outerWidth();
                var currentRightPanelLeft = $rightPanel.position().left;
                var newPosition = currentRightPanelLeft + diff;

                if (($leftPanel.width() >= MIN_WIDTH && diff < 0) || ($rightPanel.width() >= MIN_WIDTH && diff > 0)) {
                    $leftPanel.css('right', (currentLeftPanelRight - diff) + 'px');
                    $rightPanel.css('left', (currentRightPanelLeft + diff) + 'px');
                }

                _.each($scope.editors, function(editor) {
                    editor.resize();
                });
            }

            function getVisiblePanels() {
                return _.filter($scope.panels, function(panel, id) {
                    return panel.style && panel.visible;
                });
            }

            function getLastVisiblePanel() {
                return getVisiblePanels().pop();
            }

            function setEditorWidths() {
                var visibleStretchPanels = getVisiblePanels(),
                    lastVisiblePanel = getLastVisiblePanel(),
                    panelIterator = 0,
                    panelWidths = Math.floor(100 / visibleStretchPanels.length),
                    $contentArea = AJS.$('#sandbox-content-area'),
                    $library = AJS.$('#library-nav');

                // The library doesn't count as a stretch panel.
                if ($scope.panels.library.visible) {
                    $contentArea.css('left', $library.outerWidth());
                } else {
                    $contentArea.css('left', 0);
                }

                _.each(visibleStretchPanels, function(panel, id) {
                   panel.style.left = (panelWidths * panelIterator) + '%';

                   // Need to floor the calc so it there's no gap on the right for values like 33%.
                   panel.style.right = 100 - (panelWidths * (panelIterator + 1)) + '%';
                   panelIterator++;
                });

                // Always set the right most panel to 0 so there's no gap.
                if (lastVisiblePanel) {
                    lastVisiblePanel.style.right = 0;
                }
            }

            function resizeEditors() {
                _.defer(function() {
                    _.each(editorService.getEditors(), function(editor) {
                        if (editor) {
                            editor.resize();
                        }
                    });
                });
            }

            function resizeEditorsOnWindowResize() {
                AJS.$(window).resize(function(e) {
                    setEditorWidths();
                    resizeEditors();
                });
            }

            function noSoyInProd() {
                if (!envService.isHttp()) {
                    delete $scope.panels.soy;
                }
            }

            function savePanels() {
                if (localStorage) {
                    localStorage.setItem('toggledStretchPanels', JSON.stringify($scope.panels));
                }
            }

            function restoreSavedPanels() {
                if (localStorage) {
                    var lstoggles = localStorage.getItem('toggledStretchPanels');

                    if (lstoggles) {
                        var storedToggles = JSON.parse(lstoggles);

                        for (var i in $scope.panels) {
                            angular.extend($scope.panels[i], storedToggles[i]);
                        }

                        setEditorWidths();
                    } else {
                        localStorage.setItem('toggledStretchPanels', JSON.stringify($scope.panels));
                    }
                }
            }

            function checkNoJavascript() {
                if ($scope.editors.js.getValue() === '') {
                    AJS.$('#run-js-button').attr('aria-disabled', 'true');
                } else if (AJS.$('#run-js-button').attr('aria-disabled') === 'true') {
                    AJS.$('#run-js-button').attr('aria-disabled', 'false');
                }
            }

            function checkNoJavascriptOnJsChange() {
                $scope.editors.js.on('change', checkNoJavascript);
            }
        }
    ];
});
