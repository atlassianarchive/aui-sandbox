define(['services/config'], function(config) {
    function buildComponent(component) {
        component.displayName = function displayName() {
            var parts = this.name.split(/([A-Z][a-z]+)/);
            parts[0] = parts[0].charAt(0).toUpperCase() + parts[0].substring(1);
            return parts.join(' ');
        };

        return component;
    }

    return [
        '$scope',
        '$routeParams',
        '$http',
        'saveService',
        'editorService',
        'componentService',
        function(scope, params, http, saveService, editorService, componentService) {
            scope.editors = editorService;
            scope.hash = params.hash;
            scope.version = params.version;

            if (params.hash) {
                loadSavedCode(params.hash);
            } else if (params.component){
                _.defer(function(){
                    loadComponent(params.component);
                });
            }

            loadDemos();
            loadSaveCodeIfHash();


            function loadComponent(component) {
                componentService.setCurrentComponent(component);
            }

            function loadDemos() {
                getDemos().then(function(r) {
                    _.each(r.data, function(component) {
                        scope.components[component.name] = buildComponent(component);
                    });
                });
            }

            function loadSaveCodeIfHash() {
                if (params.hash) {
                    loadSavedCode(params.hash);
                }
            }

            function loadSavedCode(hash) {
                saveService.loadCodeFromServer(hash, function(data, status, headers, config) {
                    if (data.isSoy) {
                        editorService.enableSoy();
                    }

                    editorService.setCode('html', data.html);
                    editorService.setCode('css', data.css);
                    editorService.setCode('js', data.js);
                    editorService.setCode('soy', data.soy);
                    scope.savedVersion = data.version;
                });
            }

            function getDemos() {
                return http.get(formatDemosUrl());
            }

            function formatDemosUrl() {
                var url = config.aui.demos;

                for (var a in params) {
                    url = url.replace(':' + a, params[a]);
                }

                return url;
            }
        }
    ];
});