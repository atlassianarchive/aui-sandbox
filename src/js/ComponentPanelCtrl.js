define(function() {
    function alphabetically(a, b) {
        if (a.title < b.title) {
            return -1;
        }

        if (a.title > b.title) {
            return 1;
        }

        return 0;
    }

    return [
        '$scope',
        'saveService',
        'editorService',
        'componentService',
        function(
            $scope,
            saveService,
            editorService,
            componentService
        ) {
            var editors = editorService.getEditors();

            $scope.addComponentToEditor = function(component) {
                var jsLastRow = editors.js.getLastVisibleRow() + 1,
                    htmlLastRow = editors.html.getLastVisibleRow() + 1,
                    htmlContent = $scope.soySupporter.enable ? component.soy : component.html;

                this.updateSnippetId(null);
                componentService.setCurrentComponent(component);
                $scope.runJavascript();
            };

            $scope.cleanEditors = function(data) {
                _.each(editors, function(editor) {
                    editor.setValue();
                });
            }

            $scope.runJavaScript = function() {
                var js = editors.js.getValue(),
                    iframeWindow =  AJS.$('#output-frame')[0].contentWindow;

                iframeWindow.AJS.$('<script>').html(js).appendTo(AJS.$('body', iframeWindow.contentWindow));
            }

            $scope.updateSoyFlag = function() {
                $scope.enableSoy = $scope.enableSoy ? false : true;
                $scope.toggleSoyText = $scope.enableSoy ? 'Disable' : 'Enable';
                $scope.htmlOrSoy = $scope.enableSoy ? 'soy' : 'html';
                editors.html.setValue('');
                $scope.soySupporter.enable = $scope.enableSoy;

                if (!$scope.soySupporter.enable) {
                    $scope.soySupporter.toggleError({
                        value: false
                    });
                }
            };

            $scope.isActive = function(type, component) {
                if (type === 'snippet') {
                    return component.name === $scope.currentSnippetId ? 'aui-nav-selected' : '';
                }

                if (type === 'component') {
                    return component.name === componentService.getCurrentComponent() ? 'aui-nav-selected' : '';
                }
            };

            function checkNoJavascript() {
                if (editors.js.getValue() === '') {
                    AJS.$('#run-js-button').attr('aria-disabled', 'true');
                } else if (AJS.$('#run-js-button').attr('aria-disabled') === 'true') {
                    AJS.$('#run-js-button').attr('aria-disabled', 'false');
                }
            }

            function checkNoJavascriptOnJsChange() {
                editors.js.on('change', checkNoJavascript);
            }
        }
    ];
});