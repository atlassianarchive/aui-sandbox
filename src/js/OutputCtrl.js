define(function() {
    return ['$scope', 'editorService', 'componentService', function($scope, editorService, componentService) {
        var iframeWindow, iframeDocument;
        var $head, $body, $css, $js;
        var iframeReady = false;

        $scope.reset = function() {
            // Containers to dump code into
            $head = AJS.$("head", iframeDocument);
            $body = AJS.$("body", iframeDocument);
            $css = AJS.$("<style>").appendTo($head);
            $js = null; // JS is special. Can't just dump into the same <script> tag and expect it to run.
        };


        $scope.output = function() {
            var html = editorService.getCode('html');
            var css = editorService.getCode('css');
            //change background colour of preview depending on what component it is
            var pageLevelComponents = ["pageHeader", "appheader", "horizontalNav"];
            isPageLevel = _.find(pageLevelComponents, function(component) {
                return component == componentService.getCurrentComponent();
            });

            // add line-highlight attrs
            lines = html.split("\n");
            regex = /<(\w*(?:-\w*)*)([> ])/g;
            lines = _.map(lines, function(line, index) {
                return line.replace(regex, "<$1 data-linenum=" + index + "$2");
            });
            html = lines.join("\n");

            if (isPageLevel) {
                $body.addClass("page-level");
            } else {
                $body.removeClass("page-level");
            }

            try {
                if ($scope.soySupporter.enable) {
                    $scope.handleSoyRequestThrottle();
                } else {
                    $body.html(html);
                }
                $css.html(css);
                iframeWindow.runSandboxJavasript();
            } catch(e) {
                // Ignore
            }
        };

        $scope.handleSoyRequestThrottle = function() {
            if ($scope.soyCompileTimeout) {
                clearTimeout($scope.soyCompileTimeout);
                $scope.soyCompileTimeout = null;
            }
            /*
             Send soy compile request every 1.5 seconds
             */
            $scope.soyCompileTimeout = setTimeout(function() {
                clearTimeout($scope.soyCompileTimeout);
                $scope.soySupporter.insertSoyContents({
                    editors: {
                        soy: editorService.getCode('soy'),
                        html:editorService.getCode('html'),
                        js: editorService.getCode('js')
                    },
                    iframe: {
                        iframeWindow: iframeWindow,
                        iframeDocument: iframeDocument,
                        $head: $head
                    }
                });
            }, $scope.soySupporter.soyThrottleRate);
        }

        $scope.outputAndRunJs = function() {
            $scope.reset();
            $scope.output();

            var js = editorService.getCode('js');

            if ($js) $js.remove();
            $js = iframeWindow.AJS.$('<script>').html(js).appendTo($body);
        };

        // Hover highlight code
        var hoverStack = [];

        $scope.hoverChange = function(e) {
            var line = parseInt($(e.target).data("linenum"));
            if (isNaN(line)) {
                return;
            }

            if (e.type == 'mouseenter') {
                if (hoverStack.length != 0) {
                    removeHighlight(hoverStack[hoverStack.length-1]);
                }

                hoverStack.push(+line);
                hoverStack.sort(function(a,b) {return a-b});
                addHighlight(hoverStack[hoverStack.length-1]);
            } else if (e.type == 'mouseleave') {
                var i = hoverStack.indexOf(line);
                if (i != -1) {
                    // remove the line number from the stack
                    remove = hoverStack.splice(i, hoverStack.length - i);
                    remove.forEach(removeHighlight);
                }
                if (hoverStack.length != 0) {
                    addHighlight(hoverStack[hoverStack.length-1]);
                }
            }
        }

        function removeHighlight (lineNumber) {
            var firstVisibleRow = editorService.getFirstVisibleRow('html');
            var aceLines = $('#html-editor .ace_line');
            $(aceLines[lineNumber-firstVisibleRow]).removeClass("hover-highlighted");
        }

        function addHighlight (lineNumber) {
            var firstVisibleRow = editorService.getFirstVisibleRow('html');
            var aceLines = $('#html-editor .ace_line');
            $(aceLines[lineNumber-firstVisibleRow]).addClass("hover-highlighted");
        }



        setupIframe();
        $scope.reset();
        bindRunJavascript();

        _.each(editorService.getEditors(), function(editor) {
           editor.getSession().on('change', function() {
               $scope.output();
           });
        });

        function bindRunJavascript() {
           AJS.$('body').bind('runJavascript', function() {
                $scope.outputAndRunJs();
           });
        }

        function bindToEditorChange(id, callback) {

        }

        function setupIframe() {
            // set up the guts of the iframe
            iframeWindow = AJS.$('#output-frame')[0].contentWindow;
            iframeDocument = iframeWindow.document;
            iframeDocument.write("<!DOCTYPE html>");
            iframeDocument.write("<html>");
            iframeDocument.write("<head>");

            // Inject AUI CSS
            iframeDocument.write('<link rel="stylesheet" type="text/css" href="' + auiPath('css/aui.css') + '">');
            iframeDocument.write('<link rel="stylesheet" type="text/css" href="' + auiPath('css/aui-experimental.css') + '">');
            iframeDocument.write('<link rel="stylesheet" type="text/css" href="component-output.css">');
            iframeDocument.write("</head>");
            iframeDocument.write("<body></body>");
            iframeDocument.write("</html>");

            // Can't use jQuery here as it will try to load the script with XmlHttpRequest
            var ajsScripts = [];

            ajsScripts.push('../bower_components/jquery/jquery.js');
            ajsScripts.push(auiPath('js/aui.js'));
            ajsScripts.push(auiPath('js/aui-datepicker.js'));
            ajsScripts.push(auiPath('js/aui-experimental.js'));
            ajsScripts.push(auiPath('js/aui-soy.js'));

            for (var i = 0; i < ajsScripts.length; ++i) {
                var AJSscript = iframeDocument.createElement('script');
                AJSscript.type = 'text/javascript';
                AJSscript.async = false;
                AJSscript.src = ajsScripts[i];
                AJS.$("head", iframeDocument)[0].appendChild(AJSscript);
            }

            //add custom javascript to run
            AJSscript.onload = function() {
                var componentScript = iframeDocument.createElement('script');
                componentScript.src = 'component-output.js';
                AJS.$("head", iframeDocument)[0].appendChild(componentScript);
            };
        }

        function auiPath(path) {
            return '../bower_components/aui-' + $scope.version + '/' + path;
        }
    }]
});