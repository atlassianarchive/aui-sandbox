requirejs.config({
    paths: {
        angular: '../../bower_components/angular/angular',
        aui: '../../bower_components/aui/js/aui',
        auiSoy: '../../bower_components/aui/js/aui-soy',
        auiExperimental: '../../bower_components/aui/js/aui-experimental',
        jquery: '../../bower_components/jquery/jquery',
        uiAce: '../../bower_components/angular-ui-ace/ui-ace',
        underscore: '../../bower_components/underscore/underscore',
        angularRoute: '../../bower_components/angular-route/angular-route'
    },
    shim: {
        angular: {
            exports: 'angular'
        },
        angularRoute: ['angular'],
        aui: {
            deps: ['auiSoy', 'jquery'],
            exports: 'AJS'
        },
        auiExperimental: {
            deps: ['aui']
        },
        auiSoy: {
            exports: 'aui'
        },
        jquery: {
            exports: 'jQuery'
        },
        uiAce: {
            deps: ['angular', '../../bower_components/ace-builds/src/ace']
        }
    }
});