define([
    'SoySupportCtrl',
    'MainModule'
], function(
    SoySupporter
) {
    return [
        '$http',
        '$scope',
        'editorService',
        'envService',
        'saveService',
        'componentService',
        '$routeParams',
        function($http, $scope, editorService, envService, saveService, componentService, routeParams) {
            $scope.currentComponent = null;
            $scope.currentSnippetId = null;
            $scope.components = {};
            $scope.currentSnippetId = null;
            $scope.editors = {
                html: true,
                css: true,
                js: true,
                soy: true
            };
            $scope.isSoyAvailable = envService.isHttp();
            $scope.isFlatpack = !$scope.isSoyAvailable;
            $scope.htmlOrSoy = 'HTML';
            $scope.soySupporter = SoySupporter.init();
            $scope.publishURL = '';
            $scope.canSave = false;
            $scope.toggleSoyText = 'Enable';
            $scope.enableSoy = false;
            $scope.AUIVersion = AJS.version;

            $scope.snippets = saveService.getSnippets();
            $scope.deleteSnippetDialog = new AJS.Dialog({width: 400, height: 200, id: 'delete-snippet-dialog'});
            $scope.deleteSnippetDialog.addHeader('Delete Snippet');
            $scope.deleteSnippetDialog.addPanel('Panel 1', '', 'panel-body');
            $scope.deleteSnippetDialog.addButton('Delete',  null);
            $scope.deleteSnippetDialog.addLink('Cancel',  function(dialog) {
                dialog.hide();
            }, '');

            $scope.keyboardShortcutsDialog = new AJS.Dialog({width: 600, height: 400, id: 'keyboard-shortcuts-dialog'});
            $scope.keyboardShortcutsDialog.addHeader('Sandbox Keyboard Shortcuts');
            $scope.keyboardShortcutsDialog.addPanel('Panel 1', AJS.$('#keyboard-shortcuts-dialog-content').html(), 'panel-body');
            $scope.keyboardShortcutsDialog.addLink('Cancel',  function(dialog) {
                dialog.hide();
            }, '');

            $scope.aboutDialog = new AJS.Dialog({width: 600, height: 400, id: 'keyboard-shortcuts-dialog'});
            $scope.aboutDialog.addHeader('About Sandbox');
            $scope.aboutDialog.addPanel('Panel 1', AJS.$('#about-dialog-content').html(), 'panel-body');
            $scope.aboutDialog.addLink('Close',  function(dialog) {
                dialog.hide();
            }, '');

            $scope.updateCurrentComponent = function(newId) {
                $scope.currentComponent = newId;
            };

            $scope.openSnippet = function(snippetId) {
                var snippet = saveService.getSnippetById(snippetId);

                $scope.currentSnippetId = snippet.id;
                componentService.setCurrentComponent();
                editorService.setCode('html', snippet.html);
                editorService.setCode('js', snippet.js);
                editorService.setCode('css', snippet.css);
            };

            $scope.runJavascript = function(){
                editorService.runJavascript();
            }

            $scope.updateSnippetId = function(newId) {
                $scope.currentSnippetId = newId;
            };

            $scope.reset = function() {
                componentService.setCurrentComponent();
                _.each(editorService.getEditors(), function(editor) {
                    editor.setValue('');
                });

                $scope.soySupporter.insertSoyBoilerplate(editorService.getEditor('soy'));
            };
    
            $scope.openKeyboardShortcuts = function() {
                $scope.keyboardShortcutsDialog.show();
            };
    
            $scope.openAbout = function() {
                $scope.aboutDialog.show();
            };

            $scope.hoverSnippetLink = function($event) {
                AJS.$($event.target).find('.remove-snippet-icon').removeClass('hidden');
            };
    
            $scope.unhoverSnippetLink = function($event) {
                if (!AJS.$($event.toElement).parent().is($event.target)) {
                    AJS.$($event.target).find('.remove-snippet-icon').addClass('hidden');
                }
            };

            $scope.changePublishURL = function(newURL) {
                $scope.publishURL = newURL;
            };
    
            $scope.changePublishURL = function(newURL) {
                $scope.publishURL = newURL;
            };

            $scope.checkDeleteSnippet = function(snippet, $event) {
                AJS.$($event.target).addClass('hidden');
                AJS.$($scope.deleteSnippetDialog.getPanel(0,0).body).html('Are you sure you want to delete ' + snippet.title + '?');

                var $deleteButton = AJS.$($scope.deleteSnippetDialog.page[0].button[0].item);
    
                $deleteButton.unbind('click');
                $deleteButton.click(function() {
                    $scope.deleteSnippet(snippet);
                    $scope.deleteSnippetDialog.hide();
                    AJS.$('#workspace-bar .li').click();
                });

                $scope.deleteSnippetDialog.show();
            };

            $scope.deleteSnippet = function(snippet) {
                $scope.snippets = saveService.deleteSnippet(snippet.index);
                $scope.$apply();
            };

            $scope.doesVersionMatch = function(){
                return saveService.getLoadedVersion() === routeParams.version;
            }

            $scope.createTooltip = function() {
                AJS.$(".version-warning").tooltip({aria:true});
            }

        }
    ];
});