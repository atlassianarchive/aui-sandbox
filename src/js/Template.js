define(function() {
    return {
        components: {},
        patterns: {},
        get: function(library, name) {
        	var component = this[library][name] = this[library][name] || {};
        	component["js"] = component["js"] || "";
        	component["html"] = component["html"] || "";
            component["soy"] = component["soy"] || "";
        	return component;
        },
        register: function(name, type, data, library) {
        	var component = this.get(library, name);
        	component[type] = data;
        },

        find: function(name){
            return this.components[name] || this.patterns[name] || undefined;
        }
    };
});