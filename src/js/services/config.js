define(function() {
  return {
    aui: {
      demos: 'demos/:version.json',
      version: '5.3.0'
    }
  };
});