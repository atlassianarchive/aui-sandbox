define(function() {
    var snippets = null;
    var loadedCache = {};
    return function($http, $location, $routeParams) {
        var loadSnippetsFromLocalStorage = function() {
            var snippets = localStorage.getItem('snippets');
            return snippets ? JSON.parse(snippets) : [];
        };

        var saveServiceInstance = {

            //Publishes the code to the server and returns a url
            publishCode: function(js, css, html, soy, isSoy, version, id, callback){
                var url = "/code";
                var method = "POST";

                if(id){
                    url = url + "/" + id;
                    method = "PUT"
                }
                $http({
                    url: url,
                    method: method,
                    data:{
                        js: js,
                        css: css,
                        html: html,
                        soy: soy,
                        isSoy: isSoy,
                        version: $routeParams.version
                    }
                }).success(function(data, status, headers, config) {
                    var pushStateString = data.id;
                    var newURL;
                    if(data.rev){
                        pushStateString = pushStateString + "-" + data.rev;
                    }

                    $location.path(version + "/" + pushStateString);
                    newURL = $location.absUrl();
                    if(typeof callback === "function"){
                        callback(newURL);
                    }
                })
            },
            readURL: function(url){
                 var pathSplit = url.split("/"),
                     readId = pathSplit[5];
                return readId;
            },
            loadCodeFromServer: function(id, callback){
                //Load the code for the given id if it exists
                var url = '/code/' + id;

                $http({
                    url: url,
                    method: 'GET'
                }).success(function(data, status, headers, config) {
                    loadedCache = data;
                    if (typeof callback === 'function') {
                        callback(data, status, headers, config);
                    }
                });
            },

            getLoadedVersion: function(){
                return loadedCache.version;
            },

            // Saves code to localStorage and returns the snippet (incl. id and title)
            // If snippetId is null or cannot be found, saves a new snippet.
            saveCode: function(snippetId, code) {
                var snippet;
                var snippets = saveServiceInstance.getSnippets();

                if (!snippets[snippetId-1]) {
                    var newId = saveServiceInstance.newSnippetId();
                    snippet = _.extend({}, code, {
                        id: newId,
                        title: 'Snippet ' + (newId)
                    });
                    snippets.push(snippet);
                } else {
                    _.extend(snippets[snippetId-1], code);
                }

                localStorage.setItem('snippets', JSON.stringify(snippets));
                return snippet;
            },

            deleteSnippet: function(snippetIndex){
                var snippets = saveServiceInstance.getSnippets();
                snippets.splice(snippetIndex, 1);
                localStorage.setItem('snippets', JSON.stringify(snippets));
                return snippets;
            },

            getSnippets: function() {
                return JSON.parse(localStorage.getItem('snippets')) || [];
            },

            getSnippetById: function(id) {
                var snippets = saveServiceInstance.getSnippets();
                return _.find(snippets, function(snippet) {
                    return snippet.id === id;
                });
            },
            newSnippetId: function(){
                var snippets = saveServiceInstance.getSnippets(),
                    currentMax= snippets[0];
                    if(snippets.length > 0){
                        for(i in snippets){
                            if(parseInt(snippets[i].id) > parseInt(currentMax.id)) {
                                currentMax = snippets[i];
                            }
                        }

                        return currentMax.id + 1;
                    } else {
                        return 1;
                    }
                }
            }
        return saveServiceInstance;
    };
});