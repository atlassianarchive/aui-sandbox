define(function() {
  return function() {
    return {
      isHttp: function() {
        return ['http:', 'https:'].indexOf(location.protocol) > -1;
      }
    };
  };
});