define([
    'angular',
    'aui',
    'auiExperimental',
    'libraries/underscore',
    'MainModule'
], function(ng, aui) {
    return function() {
        aui.$(function($) {
            // We need to start the app manually because we're using requirejs.
            ng.bootstrap(document,["sandbox"]);

            //show warning message if no local storage
            if (!window.localStorage) {
                aui.messages.warning("#header", {
                    id: 'no-local-storage-message',
                    title: "Local Version",
                    body: "This browser does not support local storage, you will not be able to save your code snippets",
                    insert: "prepend"
                });

                $("#snippet-buttons").hide();
            }

            // this loads after everything else, remove the loading screen here
            window.setTimeout(function() {
                var $loadingScreen = $(".loading-screen");
                $loadingScreen.addClass("hidden");
                $loadingScreen.on("transitionend webkitTransitionEnd", function() {
                    $loadingScreen.remove();
                });
            }, 500); //wait 1 second so it doesn't flash if it's too quick
        });
    };
});