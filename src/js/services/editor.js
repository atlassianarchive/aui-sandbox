define(function() {
    return function() {
        var editors = {};
        var soyEnabled = false;

        return {
            init: function(ed) {
                var id = ed.container.id.replace('-editor', '');
                editors[id] = ed;
            },
            getCode: function(ed) {
                return editors[ed].getValue();
            },
            setCode: function(ed, code) {
                if (typeof editors[ed] !== 'undefined') {
                    editors[ed].setValue(code);
                }
            },
            isSoyEnabled: function() {
                return soyEnabled;
            },
            enableSoy: function() {
                soyEnabled = true;
            },
            disableSoy: function() {
                soyEnabled = false;
            },
            toggleSoy: function() {
                if (soyEnabled) {
                    this.disableSoy();
                } else {
                    this.enableSoy();
                }
                return this;
            },
            getEditor: function(id) {
                return editors[id];
            },
            getEditors: function() {
                return editors;
            },
            runJavascript: function(){
                _.defer(function(){
                    AJS.$('body').trigger('runJavascript');
                });
            }, 
            getFirstVisibleRow: function(ed) {
                return editors[ed].getFirstVisibleRow();
            }
        };
    };
});