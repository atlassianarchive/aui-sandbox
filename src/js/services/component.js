define(function() {
    return ['editorService', function(editorService) {
        var currentComponent;
        var componentServiceInstance = {
            setCurrentComponent: function(component) {
                if (!component) {
                    editorService.setCode('html', '');
                    editorService.setCode('css', '');
                    editorService.setCode('js', '');
                    return;
                }

                //TODO: Remove this and replace with some kind of event system
                _.defer(function() {
                    editorService.setCode('html', component.html);
                    editorService.setCode('css', component.css);
                    editorService.setCode('js', component.js);
                    editorService.runJavascript();
                });
            },
            getCurrentComponent: function() {
                return currentComponent;
            }
        }

        return componentServiceInstance;
    }];
});