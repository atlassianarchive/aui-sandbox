define([
    'angular',
    'ComponentPanelCtrl',
    'MainCtrl',
    'OutputCtrl',
    'SaveCtrl',
    'SoySupportCtrl',
    'TogglePanelCtrl',
    'ThemeSwitcherCtrl',
    'URLCtrl'
], function(
    ng,
    ComponentPanelCtrl,
    MainCtrl,
    OutputCtrl,
    SaveCtrl,
    SoySupportCtrl,
    TogglePanelCtrl,
    ThemeSwitcherCtrl,
    URLCtrl
) {
    var controllers = angular.module('controllers', []);

    controllers.controller('ComponentPanelCtrl', ComponentPanelCtrl);
    controllers.controller('MainCtrl', MainCtrl);
    controllers.controller('OutputCtrl', OutputCtrl);
    controllers.controller('SaveCtrl', SaveCtrl);
    controllers.controller('SoySupportCtrl', SoySupportCtrl);
    controllers.controller('TogglePanelCtrl', TogglePanelCtrl);
    controllers.controller('ThemeSwitcherCtrl', ThemeSwitcherCtrl);
    controllers.controller('URLCtrl', URLCtrl);

    return controllers;
});