define([
    'angular',
    'modules/controllers',
    'services/editor',
    'services/env',
    'services/save',
    'services/component',
    'Template',
    'services/config',
    'underscore',
    'angularRoute',
    'uiAce'
], function(
    ng,
    controllers,
    editorService,
    envService,
    saveService,
    componentService,
    template,
    config
) {
    var sb = angular.module('sandbox', [
        'controllers',
        'ui.ace',
        'ngRoute'
    ]);

    SANDBOX = {
        Library: template
    };

    sb.factory('envService', envService);
    sb.factory('editorService', editorService);
    sb.factory('componentService', componentService);
    sb.factory('saveService', saveService);

    sb.directive('runFunction', function() {
        return {
            scope: {
                method: '&runFunction'
            },
            link: function(scope) {
                scope.method();
            }
        }
    });

    sb.config(['$routeProvider', function($routeProvider) {
        $routeProvider.
            when('/:version/component/:component', {
                templateUrl: 'src/templates/version.html',
                controller: 'URLCtrl'
            }).
            when('/:version/:hash', {
                templateUrl: 'src/templates/version.html',
                controller: 'URLCtrl'
            }).
            when('/:version', {
                templateUrl: 'src/templates/version.html',
                controller: 'URLCtrl'
            }).
            otherwise({
                redirectTo: '/' + config.aui.version
            });
    }]);

    return sb;
});