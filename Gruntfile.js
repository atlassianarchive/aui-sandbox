'use strict'

module.exports = function (grunt) {
    var path = require('path');


    function librariesFile(path) {
        return 'bower_components/' + path;
    }

    function testFile(path) {
        return 'tests/' + path;
    }

    function appFile(path) {
        return 'src/' + path;
    }


    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        availabletasks: {
            options: {
                filter: 'include',
                tasks: [
                    'build',
                    'clean',
                    'dist',
                    'server',
                    'test',
                    'test-watch'
                ]
            },
            all: {}
        },
        clean: {
            dist: ['dist'],
            tmp: ['.tmp']
        },
        concat: {
            options: {
                stripBanners: true,
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */\n\n'
            },
            all: {
                files: {
                    '.tmp/all.css': [
                        'bower_components/aui/css/aui.css',
                        'bower_components/aui/css/aui-experimental.css',
                        'src/css/main.css'
                    ]
                }
            }
        },
        copy: {
            all: {
                files: [{
                    cwd: 'src/img',
                    dest: 'dist/img',
                    expand: true,
                    flatten: true,
                    src: ['**']
                }, {
                    cwd: 'src/templates',
                    expand: true,
                    src: ['*'],
                    dest: 'dist/src/templates'
                }]
            }
        },
        cssmin: {
            all: {
                files: {
                    '.tmp/all.css': ['.tmp/all.css']
                }
            }
        },
        'http-server': {
            dev: {},
            dist: {
                options: {
                    baseUrl: 'dist/',
                    port: 9001
                }
            }
        },
        karma: {
            options: {
                reporters: ['progress'],
                frameworks: ['mocha', 'requirejs', 'chai'],
                files: [
                    //libraries
                    librariesFile('angular/angular.js'),
                    {pattern: librariesFile('**/*.js'), included: false},

                    //app
                    {pattern: appFile('js/**/*.js'), included: false},

                    //tests
                    testFile('test-main.js'),
                    {pattern: testFile('**/*.js'), included: false}
                ],
                browsers: ['PhantomJS']
            },
            run: {
                options: {
                    singleRun: true
                }
            },
            dev: {
                options: {
                    singleRun: false,
                    autoWatch: true
                }

            }
        },
        md5: {
            options: {
                encoding: null,
                keepBasename: true,
                keepExtension: true
            },
            all: {
                files: {
                    'dist/': ['.tmp/all.css', '.tmp/all.js']
                },
                options: {
                    after: function(changes, opts) {
                        grunt.config('processhtml.options.data', {
                            css: path.basename(changes[0].newPath),
                            js: path.basename(changes[1].newPath)
                        });
                    }
                }
            }
        },
        processhtml: {
            options: {
                process: true
            },
            all: {
                files: {
                    'dist/index.html': 'index.html'
                }
            }
        },
        requirejs: {
            options: {
                baseUrl: 'src/js',
                mainConfigFile: 'src/js/main.js',
                name: 'services/app',
                out: '.tmp/all.js'
            },
            all: {}
        },
        shell: {
            options: {
                failOnError: true,
                stderr: true,
                stdout: true
            },
            'soy-server': {

                command: [
                    'cd bower_components/soy-service',
                    'java -jar target/dependency/jetty-runner.jar --port 9001 target/*.war'
                ].join('&&')
            }
        },
        uglify: {
            all: {
                options:{
                    beautify : {
                        // This needs to be turned on so the unicode characters in soyutils regex aren't converted.
                        ascii_only : true
                    }
                },
                files: {
                    '.tmp/all.js': ['bower_components/requirejs/require.js', '.tmp/all.js']
                }
            }
        }
    });

    grunt.loadTasks('./build/tasks');
    grunt.loadNpmTasks('grunt-available-tasks');
    grunt.loadNpmTasks('grunt-contrib');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-md5');
    grunt.loadNpmTasks('grunt-processhtml');
    grunt.loadNpmTasks('grunt-shell');

    grunt.registerTask('build', 'Runs the tests then builds the dist.', ['test', 'dist']);
    grunt.registerTask('default', ['availabletasks']);
    grunt.registerTask('dist', 'Builds the dist.', ['clean', 'requirejs', 'uglify', 'concat', 'cssmin', 'copy', 'md5', 'processhtml', 'clean:tmp']);
    grunt.registerTask('soy-server', 'Runs the soy server.', ['shell:soy-server']);
    grunt.registerTask('test', 'Runs the unit tests.', ['karma:run']);
    grunt.registerTask('test-watch', 'Runs the unit test server for development', ['karma:dev']);
};
