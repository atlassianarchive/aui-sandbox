//Karma needs this so that it can load the tests (see deps in config below)
var tests = [];
for (var file in window.__karma__.files) {
    if (window.__karma__.files.hasOwnProperty(file)) {
        if (/base\/tests\/.*Test\.js$/.test(file)) {
            tests.push(file);
        }
    }
}

//some re-usable directories
function librariesFile(path) {
    return 'bower_components/' + path;
}

requirejs.config({
    // Karma serves files from '/base'
    baseUrl: '/base/src/js',
    paths: {
        uiAce: '/base/' + librariesFile('angular-ui-ace/ui-ace'),
        angular: '/base/'+ librariesFile('angular/angular'),
        angularMocks : '/base/' + librariesFile('angular-mocks/angular-mocks'),
        angularRoute: '/base/' + librariesFile('angular-route/angular-route'),
        underscore: '/base/' + librariesFile('underscore/underscore')

    },
    shim: {
        angular: {
            exports: 'angular'
        },
        uiAce: {
            deps: ['angular', 'libraries/ace/ace-min']
        },
        'angularMocks': {
            deps : ['angular'],
            exports:'angular.mock'
        }
    },
    // ask Require.js to load these files (all our tests)
    deps: tests,

    // start test run, once Require.js is done
    callback: window.__karma__.start
});