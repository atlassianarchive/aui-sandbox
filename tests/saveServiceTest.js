define(['angular', 'angularMocks', 'MainModule', 'underscore'], function(angular, mocks, _) {

    describe('Save Service', function() {
        describe('Publish Code', function(){
            beforeEach(function(){
                mocks.module('sandbox');
                mocks.inject(function($injector){
                    saveService = $injector.get('saveService');
                    $httpBackend = $injector.get('$httpBackend');
                });
            });

            it('should return a URL with a new ID when a new example is published',function() {
                var publishCodeResponse = {
                    id:'5abcd71fe02a218a9ecef390d60278a4'
                }

                $httpBackend.when('POST', '/code').respond(publishCodeResponse);
                saveService.publishCode('js','css','html','soy',true, '5.3', null, function(newURL){
                    chai.expect(newURL).to.equal('http://server/#/5.3/5abcd71fe02a218a9ecef390d60278a4');
                });
            });

            it('should return a URL with the old ID and revision when an update is published',function() {
                var putCodeResponse = {
                    id: '5abcd71fe02a218a9ecef390d60278a2',
                    rev: '2'
                }

                $httpBackend.when('PUT', '/code/5abcd71fe02a218a9ecef390d60278a2').respond(putCodeResponse);
                saveService.publishCode('js','css','html','soy',true, '5.3', '5abcd71fe02a218a9ecef390d60278a2', function(newURL) {
                    chai.expect(newURL).to.equal('http://server/#/5.3/5abcd71fe02a218a9ecef390d60278a2-2');
                });
            });
            it('saveService should be able to load some code from the server', function() {
                var id='5abcd71fe02a218a9ecef390d602d714';
                var getCodeResponse = {
                    html: '<html></html>'
                }

                $httpBackend.when('GET', '/code/5abcd71fe02a218a9ecef390d602d714').respond(getCodeResponse);
                saveService.loadCodeFromServer(id, function(data){
                    chai.expect(data).to.equal({html:'<html></html>'});
                })
            });
        });

        describe('Utilities', function() {
            it('saveService should be able to read a URL and return the id', function() {
                var result = saveService.readURL('http://localhost:9000/#/5.3/5abcd71fe02a218a9ecef390d602d714');
                chai.expect(result).to.equal('5abcd71fe02a218a9ecef390d602d714');
            });
        });

        describe('Snippets', function() {
            //define some test data for snippets
            var snippets = [{
                html: '<html></html>',
                js: '',
                css: '',
                id: 1,
                title: 'Snippet 1'
            }, {
                html: '<html></html>',
                js: 'alert("hello")',
                css: '',
                id: 2,
                title: 'Snippet 2'
            }]

            var newSnippet = {
                html: '',
                js: 'alert("hello3")',
                css: '',
                id: 3,
                title: 'Snippet 3'
            };

            var updatedSnippetCode = {
                html: '<html></html>',
                js: 'alert("hello2")',
                css: ''
            }

            beforeEach(function() {
                localStorage.clear();
                localStorage.setItem('snippets', JSON.stringify(snippets));
            });

            it('saveService should be able to update a localStorage snippet', function() {

                saveService.saveCode(1, updatedSnippetCode);
                var snippets = JSON.parse(localStorage.getItem('snippets'));
                chai.expect(snippets[0].html).to.equal('<html></html>');
                chai.expect(snippets[0].css).to.equal('');
                chai.expect(snippets[0].js).to.equal('alert("hello2")');
                chai.expect(snippets[0].id).to.equal(1);
                chai.expect(snippets[0].title).to.equal('Snippet 1');
            });


            it('saveService should be able to save a new localStorage snippet', function() {
                var newSnippetCode = {
                    html: newSnippet.html,
                    css: newSnippet.css,
                    js: newSnippet.js
                }

                saveService.saveCode(null, newSnippetCode);
                var snippets = JSON.parse(localStorage.getItem('snippets'));
                chai.expect(snippets[2].html).to.equal('');
                chai.expect(snippets[2].css).to.equal('');
                chai.expect(snippets[2].js).to.equal('alert("hello3")');
                chai.expect(snippets[2].id).to.equal(3);
                chai.expect(snippets[2].title).to.equal('Snippet 3');
            });

            it('saveService should be able to retrieve all snippets', function() {
                var resultSnippets = saveService.getSnippets();
                var expectedSnippets =  JSON.parse(localStorage.getItem('snippets'));
                chai.expect(resultSnippets).to.deep.equal(expectedSnippets);
            });

            it('saveService should be able to retrieve specific snippets', function() {
                var resultSnippet = saveService.getSnippetById(1);
                chai.expect(resultSnippet.html).to.equal('<html></html>');
                chai.expect(resultSnippet.css).to.equal('');
                chai.expect(resultSnippet.js).to.equal('');
                chai.expect(resultSnippet.id).to.equal(1);
                chai.expect(resultSnippet.title).to.equal('Snippet 1');
            });

            it('saveService should be able to delete snippets', function() {
                saveService.deleteSnippet(0);
                var snippet = saveService.getSnippetById(1);
                chai.expect(snippet).to.equal(undefined);
            });

            it('saveService should be able to generate a new snippet ID', function() {
                var newId = saveService.newSnippetId();
                chai.expect(newId).to.equal(3);
            });
        });
    });
});
